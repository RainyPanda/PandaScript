const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('hello')
		.setDescription('Replies with a greeting')
		.addStringOption(option =>
			option
			.setName('content')
			.setDescription('The content for the reply')
			.setRequired(true)),

	async execute(interaction) {
		const reply_content = interaction.options.getString('content')
		await interaction.reply(reply_content);
	},
};