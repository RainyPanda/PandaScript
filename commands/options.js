const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
	.setName('options')
	.setDescription('A command with options')
	.addStringOption(option =>
		option.setName('input')
			.setDescription('Some input'))
	.addBooleanOption(option =>
		option.setName('boolean')
			.setDescription('boolean (obviously)'))
	.addChannelOption(option =>
		option.setName('channel')
			.setDescription('a channel'))
};