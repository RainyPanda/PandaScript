const { ActionRowBuilder, ButtonBuilder, ButtonStyle, Events } = require('discord.js');

module.exports = {
	name: Events.InteractionCreate,

	async execute(interaction) {
		if (!interaction.isChatInputCommand()) return;

		if (interaction.commandName === 'hidden') {
			await interaction.reply({ content: 'Ooh a hidden message...', ephemeral: true });
			await interaction.editReply({ content: 'Still hidden...', ephemeral: true });
			await interaction.followUp({ content: 'A reply!', ephemeral: true });
		}

		if (interaction.commandName === 'button') {
		const row = new ActionRowBuilder()
			.addComponents(
				new ButtonBuilder()
					.setCustomId('primary')
					.setLabel('click?')
					.setStyle(ButtonStyle.Primary),
			);

			await interaction.reply({ content: '*click click*', components: [row] });
		};

		const command = interaction.client.commands.get(interaction.commandName);

		if (!command) {
			console.error(`No command matching ${interaction.commandName} was found.`);
			return;
		}

		try {
			await command.execute(interaction);
		} catch (error) {
			console.error(`Error executing ${interaction.commandName}`);
			console.error(error);
		}
	},
};
